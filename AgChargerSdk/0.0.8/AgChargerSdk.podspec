#
#  Be sure to run `pod spec lint AgChargerSdk.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "AgChargerSdk"
  spec.version      = "0.0.8"
  spec.summary      = "iOS SDK for autogreen chargers"

  spec.description  = "Autogreen Charger Framework to control Autogreen Chargers"

  spec.homepage     = "https://bitbucket.org/buzzpoem/agpodspecs/"

  spec.license = { :type => "MIT" }
  spec.author             = { "Leo Tam" => "leotam@buzz-code.com" }
  spec.source       = { :http => "https://bitbucket.org/buzzpoem/agpodspecs/downloads/AgChargerFramework.zip" }
  spec.ios.deployment_target = '10.0'
  spec.ios.vendored_frameworks = 'AgChargerSdk.framework'


end
